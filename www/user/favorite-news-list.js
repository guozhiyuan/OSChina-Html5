define(['text!user/favorite-news-list.html', "../base/openapi", '../base/util'],
	function(viewTemplate, OpenAPI, Util) {
		return Piece.View.extend({
			id: 'user-favorite-news-list ',
			events: {
				"click .backBtn": "goBack",
				"click .favoriteList": "goToNewDetail",
			},
			goBack: function() {
				this.navigate("user-info", {
					trigger: true
				});
			},
			goToNewDetail: function(el) {
				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");
				var fromType = 4;
				var checkDetail = "news/news-detail";
				var com = 1;
				this.navigateModule("news/news-detail?id=" + id + "&fromType=" + fromType + "&checkDetail=" + checkDetail + "&com=" + com, {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var user_token = Piece.Store.loadObject("user_token");
				var access_token = user_token.access_token;
				Util.loadList(this, 'user-favorite-news-list', OpenAPI.favorite_list, {
					'type': 4,
					'page': 1,
					'pageSize': OpenAPI.pageSize,
					'access_token': access_token,
					'dataType': OpenAPI.dataType
				});
				//write your business logic here :)
			}
		}); //view define

	});